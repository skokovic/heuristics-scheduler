#!/bin/bash

for dir in instances/instance*; do
    if [[ -d "$dir" ]]; then
        cd "$dir"
        for fileName in *; do
            if [[ "$fileName" =~ .*\[.*\].csv ]]; then
                newName=`echo "$fileName" | sed -E 's/(.*)\[.*\].csv/\1.csv/'`
                mv "$fileName" "$newName"
            fi
        done
        cd ./../..
    fi
done

ls -Ral instance*