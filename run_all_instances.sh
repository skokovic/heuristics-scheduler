#!/bin/bash

# pretpostavka: container mountan na ./instances/:/data, name: sheduler, workdir /data
#               postoje instance2, instance3, instance4, instance5 u direktoriju ./instances
# e.g. docker run -td -v `pwd`/instances:/data --name scheduler student-swap

# Ocekivano vrijeme trajanja: 6h 40min

mkdir instances/instance{2,3,4,5}/{10,30,60}min

# Function that schedules one run and moves files to the correct instance dir
function executeIter {

    instance=$1
    timeout=$2

    min=$(( timeout / 60 ))

    docker exec -ti -w /data scheduler /bin/sh -c "java -jar /app/scheduler.jar -timeout ${timeout} -students-file instance${instance}/student.csv -requests-file instance${instance}/requests.csv -overlaps-file instance${instance}/overlaps.csv -limits-file instance${instance}/limits.csv"

    mv "instances/results${timeout}.txt" "instances/instance${instance}/${min}min/"
    mv "instances/student_output.csv" "instances/instance${instance}/${min}min/student.csv"

}

for ins in "2" "3" "4" "5"; do
    for t in "600" "1800" "3600"; do
        executeIter "$ins" "$t"
    done
done




