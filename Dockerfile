FROM maven:3.6.0-jdk-8-alpine AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8-alpine
COPY --from=build /usr/src/app/target/heuristic-scheduler-1.0-SNAPSHOT-jar-with-dependencies.jar /app/scheduler.jar
WORKDIR /data





