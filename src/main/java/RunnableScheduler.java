import com.hr.fer.hmo.models.Activity;
import com.hr.fer.hmo.models.Group;
import com.hr.fer.hmo.models.Score;
import com.hr.fer.hmo.models.Student;

import java.util.*;

class RunnableScheduler {

    private int totalSwaps;
    private int swapsPerIteration;

    private long tabooIteration;

    /**
     * A form of taboo list, when an ID is picked,
     * it's removed from the list and can't be picked again as a starting point
     */
    private ArrayList<Integer> studentsToVisit;
    private ArrayList<Integer> studentsWithRequests;
    private HashMap<Integer, Student> studentsById;
    private HashMap<Integer, Group> groupsById;
    private HashMap<Integer, ArrayList<Integer>> waitList;

    RunnableScheduler(long tabooIteration, HashMap<Integer, Student> studentsById, HashMap<Integer, Group> groupsById) {
        totalSwaps = 0;
        swapsPerIteration = 0;
        this.tabooIteration = tabooIteration;
        this.studentsById = copyStudentsById(studentsById);
        this.groupsById = copyGroupsById(groupsById);
        studentsToVisit = new ArrayList<Integer>(studentsById.keySet());
        studentsWithRequests = new ArrayList<Integer>(studentsById.keySet());
        waitList = new HashMap<Integer, ArrayList<Integer>>();
    }

    private HashMap<Integer, Student> copyStudentsById(HashMap<Integer, Student> studentsById) {
        HashMap<Integer, Student> newStudentsById = new HashMap<Integer, Student>();
        for (Student student : studentsById.values()) {
            newStudentsById.put(student.getStudentId(), new Student(student));
        }
        return newStudentsById;
    }

    private HashMap<Integer, Group> copyGroupsById(HashMap<Integer, Group> groupsById) {
        HashMap<Integer, Group> newGroupsById = new HashMap<Integer, Group>();
        for (Group group : groupsById.values()) {
            newGroupsById.put(group.getGroupId(), new Group(group));
        }
        return newGroupsById;
    }

    /**
     * Searching a neighbourhood of random student ids
     * @return student ID with max number of activity requests in neighbourhood
     */
    private int searchNeighbourhood(boolean checkTabuList) {
        int neighbourhoodSize = adjustNeighbourhoodSize();
        Random random = new Random();
        int currentIndex = random.nextInt(studentsToVisit.size());
        if (checkTabuList) {
            while (Scheduler.tabooStudentIds.contains(currentIndex)) {
                currentIndex = random.nextInt(studentsToVisit.size());
            }
        }
        int currentPickedId = studentsToVisit.get(currentIndex);
        int maxNumActivityRequests = studentsById.get(currentPickedId).getActivities().size();
        int indexOfMax = currentIndex;
        int idOfMax = currentPickedId;
        for (int i = 0; i < neighbourhoodSize - 1; i++) {
            currentIndex = random.nextInt(studentsToVisit.size());
            currentPickedId = studentsToVisit.get(currentIndex);
            if (studentsById.get(currentPickedId).getActivities().size() > maxNumActivityRequests) {
                currentPickedId = studentsToVisit.get(currentIndex);
                maxNumActivityRequests = studentsById.get(currentPickedId).getActivities().size();
                indexOfMax = currentIndex;
                idOfMax = currentPickedId;
            }
        }
        studentsToVisit.remove(indexOfMax);
        return idOfMax;
    }

    private int adjustNeighbourhoodSize() {
        for (int i = 10; i >= 0; i--) {
            if ((studentsToVisit.size() % i) == 0) {
                return i;
            }
        }
        return 0;
    }

    private int handleSwapRequests(int studentId) {
        int numSwaps = 0;
        Student student = studentsById.get(studentId);
        for (Activity activity : student.getActivities().values()) {
            /*if (numSwaps >= Scheduler.ar.awardActivity.size()) {
                break;
            } else {*/
                numSwaps += swapGroups(activity);
            //}
        }

        return numSwaps;
    }

    private int swapGroups(Activity activity) {
        int swapped = 0;
        int oldGroupId = activity.getOldGroupId();
        for (Integer newGroupId : activity.getRequestedGroupIds()) {
            if (groupHasCollision(activity.getStudentId(), newGroupId)) {
                continue;
            } else if (!groupLimitsSatisfied(oldGroupId, newGroupId)) {
                swapped = checkWaitList(oldGroupId, newGroupId, activity);
                if (swapped == 1) {
                    break;
                } else {
                    goToWaitList(newGroupId, activity);
                }
            } else {
                activity.setNewGroupId(newGroupId);
                groupsById.get(oldGroupId).reduceStudentsCount();
                groupsById.get(newGroupId).increaseStudentsCount();
                if (waitList.containsKey(newGroupId) && (waitList.get(newGroupId).size() > 0)) {
                    Iterator<Integer> iterator = waitList.get(newGroupId).iterator();
                    int studentId;
                    while (iterator.hasNext()) {
                        studentId = iterator.next();
                        if (studentId == activity.getStudentId()) {
                            iterator.remove();
                            break;
                        }
                    }
                }
                swapped = 1;
                break;
            }
        }
        if (swapped == 1) {
            activity.removeRequests();
        }
        return swapped;
    }

    private int checkWaitList(int oldGroupId, int newGroupId, Activity activity) {
        if (waitList.containsKey(oldGroupId) && (waitList.get(oldGroupId).size() > 0)) {
            Iterator<Integer> iterator = waitList.get(oldGroupId).iterator();
            while (iterator.hasNext()) {
                int studentId = iterator.next();
                if (studentsById.get(studentId).getActivities().get(activity.getActivityId()).getCurrentGroupId() == newGroupId) {
                    activity.setNewGroupId(newGroupId);
                    studentsById.get(studentId).getActivities().get(activity.getActivityId()).setNewGroupId(oldGroupId);
                    studentsById.get(studentId).getActivities().get(activity.getActivityId()).removeRequests();
                    iterator.remove();
                    return 1;
                }
            }
        }
        return 0;
    }

    private void goToWaitList(int newGroupId, Activity activity) {
        if (!waitList.containsKey(newGroupId)) {
            waitList.put(newGroupId, new ArrayList<Integer>(activity.getStudentId()));
        } else {
            waitList.get(newGroupId).add(activity.getStudentId());
        }
    }


    private boolean groupHasCollision(int studentId, int groupId) {
        ArrayList<Integer> newGroupOverlaps = groupsById.get(groupId).getOverlaps();
        for (Activity activity : studentsById.get(studentId).getActivities().values()) {
            if (newGroupOverlaps.contains(activity.getCurrentGroupId())) {
                return true;
            }
        }
        return false;
    }

    private boolean groupLimitsSatisfied(int oldGroupId, int newGroupId) {
        Group oldGroup = groupsById.get(oldGroupId);
        Group newGroup = groupsById.get(newGroupId);
        return (oldGroup.getStudentsCount() - 1) >= oldGroup.getMinStudents()
                && (newGroup.getStudentsCount() + 1) <= newGroup.getMaxStudents();
    }

    Score run() {
        System.out.println("=====   Iteration number: " + tabooIteration + "   ======");
        int studentId = searchNeighbourhood(true);
        Scheduler.tabooStudentIds.add(studentId);
        do {
            swapsPerIteration = 0;
            studentsWithRequests.clear();
            for (Student student : studentsById.values()) {
                for (Activity activity : student.getActivities().values()) {
                    if (!activity.getRequestedGroupIds().isEmpty()) {
                        studentsWithRequests.add(student.getStudentId());
                        studentsToVisit.add(student.getStudentId());
                        break;
                    }
                }
            }
            do {
                swapsPerIteration += handleSwapRequests(studentId);
                totalSwaps += swapsPerIteration;
                studentId = searchNeighbourhood(false);
            } while (studentsToVisit.size() > 0);
        } while (swapsPerIteration != 0);

        Score score = evaluateSolution(studentsById, groupsById, Scheduler.ar.awardActivity, Scheduler.ar.awardStudent, Scheduler.ar.minMaxPenalty);
        score.setNumberOfSwaps(totalSwaps);
        score.setStudentFileOutput(generateCurrentBestOutput());

        System.out.println(score);
        System.out.println();

        return score;
    }

    private static Score evaluateSolution(HashMap<Integer,Student> studentsByIdH, HashMap<Integer,Group> groupsById, List<Integer> awardActivityValues, int awardStudent, int minMaxPenalty) {

        for(Student student : studentsByIdH.values()) {
            for (Activity activity : student.getActivities().values()) {
                activity.removeRequests();
            }
        }
        Scheduler.readRequestsFile(studentsByIdH, Scheduler.ar.requestsFileName);
        TreeMap<Integer, Student> studentsById = new TreeMap<Integer, Student>(studentsByIdH);

        int scoreA = 0;
        int scoreB = 0;
        int scoreC = 0;
        int scoreD = 0;
        int scoreE = 0;

        int lastExaminedId = 0;
        int swapedItemsForASingleStudent = 0;
        int studentsWithAllSwaps = 0;
        int maxAwardedActivityCombos = awardActivityValues.size();

        TreeMap<Integer, Integer> totalRequestedActivitesForSwap = new TreeMap<Integer, Integer>();
        int requestsCount = 0;
        for (Student student : studentsById.values()) {
            for (Activity activity : student.getActivities().values()){
                requestsCount += activity.getRequestedGroupIds().size();
            }
            totalRequestedActivitesForSwap.put(student.getStudentId(), requestsCount);
            requestsCount = 0;
        }

        // Artificial student
        Activity activityA = new Activity(0, 0, 0, 0);
        activityA.setNewGroupId(0);
        Student studentA = new Student(0);
        studentA.addToActivities(activityA);

        // Add artificial last student at the end so that foreach gets the real last student
        studentsById.put(0, studentA);
        // Check student solutions (scores A, B and C)
        for (Student student : studentsById.values()) {
            for (Activity activity : student.getActivities().values()) {
                // Next student
                if (student.getStudentId() != lastExaminedId) {
                    // Swaps were made for previous student
                    if (swapedItemsForASingleStudent > 0) {
                        // Take the combo value or greatest value available (eg. if combo values are 1,2,3 and there are 4 swaps made, then take the 3rd one)
                        int awardedScores = Math.min(maxAwardedActivityCombos, swapedItemsForASingleStudent);
                        scoreB += awardActivityValues.get(awardedScores - 1);

                        // All swaps were made for previous student
                        // Note: all swaps are calculated as the number of different activities for which the request was made, not
                        //       as a total number of activities the student is enrolled into
                        if (swapedItemsForASingleStudent == totalRequestedActivitesForSwap.get(lastExaminedId)) {
                            studentsWithAllSwaps++;

                        }
                    }
                    swapedItemsForASingleStudent = 0;
                    lastExaminedId = student.getStudentId();
                }
                if (activity.getNewGroupId() == 0 || activity.getNewGroupId() == activity.getOldGroupId()) continue;

                // Swap was made - award it
                scoreA += activity.getSwapWeight();
                swapedItemsForASingleStudent++;
            }
        }
        scoreC += awardStudent * studentsWithAllSwaps;

        int val;
        // Check group limits
        for(Group group : groupsById.values()){
            if (group.getStudentsCount() < group.getMinPreferredStudents()){
                val = (group.getMinPreferredStudents() - group.getStudentsCount()) * minMaxPenalty;
                scoreD += val;
            }

            if (group.getStudentsCount() > group.getMaxPreferredStudents()){
                val = (group.getStudentsCount() - group.getMaxPreferredStudents()) * minMaxPenalty;
                scoreE += val;
            }
        }

        return new Score(scoreA, scoreB, scoreC, scoreD, scoreE);

    }

    private String generateCurrentBestOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append("student_id");
        sb.append(',');
        sb.append("activity_id");
        sb.append(',');
        sb.append("swap_weight");
        sb.append(',');
        sb.append("group_id");
        sb.append(',');
        sb.append("new_group_id");
        sb.append('\n');

        for (Student student : studentsById.values())
            for (Activity activity : studentsById.get(student.getStudentId()).getActivities().values()) {
                sb.append(student.getStudentId());
                sb.append(',');
                sb.append(activity.getActivityId());
                sb.append(',');
                sb.append(activity.getSwapWeight());
                sb.append(',');
                sb.append(activity.getOldGroupId());
                sb.append(',');
                sb.append(activity.getNewGroupId());
                sb.append('\n');
            }
        return sb.toString();
    }

}
