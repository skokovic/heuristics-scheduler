import com.beust.jcommander.JCommander;
import com.hr.fer.hmo.arguments.Settings;
import com.hr.fer.hmo.models.Activity;
import com.hr.fer.hmo.models.Group;
import com.hr.fer.hmo.models.Score;
import com.hr.fer.hmo.models.Student;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class Scheduler {

    static Settings ar;

    static ArrayList<Integer> tabooStudentIds;
    static int tabooListSize = 100;

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();
        ar = new Settings();
        processCommandLineArguments(args);

        HashMap<Integer, Student> studentsById = new HashMap<Integer, Student>();
        HashMap<Integer, Group> groupsById = new HashMap<Integer, Group>();

        tabooStudentIds = new ArrayList<Integer>();

        readStudentsFile(studentsById, ar.studentsFileName);
        readRequestsFile(studentsById, ar.requestsFileName);
        readLimitsFile(groupsById, ar.limitsFileName);
        readOverlapsFile(groupsById, ar.overlapsFileName);

        long iterations = 0;
        Score bestScore = new Score(-100000, -10000, -10000, 10000, 10000);

        Score currentScore;
        while ((System.currentTimeMillis() - startTime)/1000 < ar.timeout) {
            if (tabooStudentIds.size() == tabooListSize) {
                tabooStudentIds.remove(0);
            }
            RunnableScheduler scheduler = new RunnableScheduler(iterations, studentsById, groupsById);
            currentScore = scheduler.run();
            if (currentScore.compareTo(bestScore) > 0) {
                bestScore = currentScore;
            }
            iterations++;
        }

        System.out.println("Best score is:\n");
        System.out.println(bestScore);
        writeStudentFile(bestScore);
        writeOutputFile(bestScore, iterations);


        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Elapsed time: " + elapsedTime/1000);
    }

    private static void processCommandLineArguments(String[] args) {
        JCommander commander
                = JCommander.newBuilder()
                .programName("Student scheduler")
                .addObject(ar)
                .build();
        commander.parse(args);

        if (ar.help) {
            commander.usage();
            System.exit(1);
        }
    }

    private static void readStudentsFile(HashMap<Integer, Student> studentsById, String studentFileName) {
        File studentsFile = new File(studentFileName);
        BufferedReader reader;
        String line;
        String[] lineSplit;
        int studentId, activityId, currentGroupId, swapWeight;

        try {
            reader = new BufferedReader(new FileReader(studentsFile));
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                lineSplit = line.split(",");

                studentId = Integer.parseInt(lineSplit[0]);
                activityId = Integer.parseInt(lineSplit[1]);
                swapWeight = Integer.parseInt(lineSplit[2]);
                currentGroupId = Integer.parseInt(lineSplit[3]);

                if (!studentsById.containsKey(studentId)) {
                    studentsById.put(studentId, new Student(studentId));
                }
                studentsById.get(studentId).addToActivities(new Activity(studentId, activityId, currentGroupId, swapWeight));
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error while reading file!");
            e.printStackTrace();
        }
    }

    static void readRequestsFile(HashMap<Integer, Student> studentsById, String requestsFileName) {
        File requestsFile = new File(requestsFileName);
        BufferedReader reader;
        String line;
        String[] lineSplit;
        int studentId, activityId, requestGroupId;

        try {
            reader = new BufferedReader(new FileReader(requestsFile));
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                lineSplit = line.split(",");

                studentId = Integer.parseInt(lineSplit[0]);
                activityId = Integer.parseInt(lineSplit[1]);
                requestGroupId = Integer.parseInt(lineSplit[2]);

                studentsById.get(studentId).getActivities().get(activityId).addToRequestedGroupIds(requestGroupId);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error while reading file!");
            e.printStackTrace();
        }
    }

    private static void readOverlapsFile(HashMap<Integer, Group> groupsById, String overlapsFileName) {
        File overlapsFile = new File(overlapsFileName);
        BufferedReader reader;
        String line;
        String[] group;

        try {
            reader = new BufferedReader(new FileReader(overlapsFile));
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                group = line.split(",");
                int group1 = Integer.parseInt(group[0]);
                int group2 = Integer.parseInt(group[1]);

                if (groupsById.containsKey(group1) && groupsById.containsKey(group2)) {
                    groupsById.get(group1).addToOverlaps(group2);
                    groupsById.get(group2).addToOverlaps(group1);
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error while reading file!");
            e.printStackTrace();
        }

    }

    private static void readLimitsFile(HashMap<Integer, Group> groupsById, String limitsFileName) {

        File limitsFile = new File(limitsFileName);
        BufferedReader reader;
        String line;
        String[] lineSplit;
        int groupId;

        try {
            reader = new BufferedReader(new FileReader(limitsFile));
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                lineSplit = line.split(",");

                groupId = Integer.parseInt(lineSplit[0]);
                groupsById.put(groupId, new Group(
                        groupId,
                        Integer.parseInt(lineSplit[1]), // current students count
                        Integer.parseInt(lineSplit[2]), // min
                        Integer.parseInt(lineSplit[3]), // min preferred
                        Integer.parseInt(lineSplit[4]), // max
                        Integer.parseInt(lineSplit[5]) // max preferred
                ));
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error while reading file!");
            e.printStackTrace();
        }
    }

    private static void writeOutputFile(Score s, long i){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new File("results" + ar.timeout + ".txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        sb.append(s.toString());
        sb.append("\nNumber of iterations:\n");
        sb.append(i);
        sb.append('\n');
        if (writer != null) {
            writer.write(sb.toString());
            writer.close();
        }
    }


    private static void writeStudentFile(Score s) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new File("student_output.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (pw != null) {
            pw.write(s.getStudentFileOutput());
            pw.close();
        }

    }
}
