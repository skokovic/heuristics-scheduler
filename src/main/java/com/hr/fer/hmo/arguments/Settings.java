package com.hr.fer.hmo.arguments;

import com.beust.jcommander.*;

import java.util.Arrays;
import java.util.List;


public class Settings {

    @Parameter(names = "-timeout", description = "In seconds, how long does the program run before returning a result")
    public Integer timeout = 600;

    @Parameter(names = "-award-activity", description = "Points for successful swaps done for a single student")
    public List<Integer> awardActivity = Arrays.asList(1, 2, 3, 4, 5);

    @Parameter(names = "-award-student", description = "Points for completing all requests for a single student")
    public Integer awardStudent = 1;

    @Parameter(names = "-minmax-penalty", description = "Points deducted for not optimal student groups")
    public Integer minMaxPenalty = 1;

    @Parameter(names = "-students-file", description = "File containing data about students (and groups), also an output file", required = true)
    public String studentsFileName = "";

    @Parameter(names = "-requests-file", description = "File containing data about student swap requests", required = true)
    public String requestsFileName = "";

    @Parameter(names = "-overlaps-file", description = "File containing data about groups who can't overlap", required = true)
    public String overlapsFileName = "";

    @Parameter(names = "-limits-file", description = "File containing data about group limits", required = true)
    public String limitsFileName = "";

    @Parameter(names = "--help", description = "Prints description of all input arguments", help = true)
    public boolean help;
}
