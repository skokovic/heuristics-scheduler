package com.hr.fer.hmo.models;

import java.util.ArrayList;

public class Activity {
    private int studentId;
    private int activityId;
    private int oldGroupId;
    private int newGroupId;
    private int currentGroupId;
    private int swapWeight;
    private ArrayList<Integer> requestedGroupIds;

    public Activity(int studentId, int activityId, int oldGroupId, int swapWeight) {
        this.studentId = studentId;
        this.activityId = activityId;
        this.oldGroupId = oldGroupId;
        this.currentGroupId = oldGroupId;
        this.swapWeight = swapWeight;
        requestedGroupIds = new ArrayList<Integer>();
    }

    public int getStudentId() {
        return studentId;
    };

    public int getActivityId() {
        return activityId;
    }

    public int getOldGroupId() {
        return oldGroupId;
    }

    public int getNewGroupId() {
        return newGroupId;
    }

    public int getCurrentGroupId() {
        return currentGroupId;
    }

    public int getSwapWeight() {
        return swapWeight;
    }

    public ArrayList<Integer> getRequestedGroupIds() {
        return requestedGroupIds;
    }

    public void addToRequestedGroupIds(int id) {
        this.requestedGroupIds.add(id);
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activityId=" + activityId +
                ", oldGroupId=" + oldGroupId +
                ", requestedGroupIds=" + requestedGroupIds +
                '}';
    }

    public void setNewGroupId(int newGroupId) {
        this.newGroupId = newGroupId;
        this.currentGroupId = newGroupId;
    }

    public void setRequestedGroupIds(ArrayList<Integer> requestedGroupIds) {
        this.requestedGroupIds = requestedGroupIds;
    }

    public void removeRequests() {
        requestedGroupIds.clear();
    }
}
