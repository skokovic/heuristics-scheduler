package com.hr.fer.hmo.models;

public class Score implements Comparable<Score>{

    private int scoreA;
    private int scoreB;
    private int scoreC;
    private int scoreD;
    private int scoreE;
    private int totalScore;
    private int numberOfSwaps;
    private String studentFileOutput;

    public Score(int scoreA, int scoreB, int scoreC, int scoreD, int scoreE) {
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.scoreC = scoreC;
        this.scoreD = scoreD;
        this.scoreE = scoreE;
        this.totalScore = scoreA + scoreB + scoreC - scoreD - scoreE;
    }

    public int compareTo(Score o) {
        return Integer.valueOf(this.totalScore).compareTo(o.totalScore);
    }

    public int getTotalScore() {
        return totalScore;
    }

    public int getNumberOfSwaps() {
        return numberOfSwaps;
    }

    public void setNumberOfSwaps(int numberOfSwaps) {
        this.numberOfSwaps = numberOfSwaps;
    }
    public String getStudentFileOutput() {
        return studentFileOutput;
    }

    public void setStudentFileOutput(String studentFileOutput) {
        this.studentFileOutput = studentFileOutput;
    }

    public int getScoreA() {
        return scoreA;
    }


    public int getScoreB() {
        return scoreB;
    }


    public int getScoreC() {
        return scoreC;
    }


    public int getScoreD() {
        return scoreD;
    }


    public int getScoreE() {
        return scoreE;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Solution values:\n");
        sb.append("ScoreA: ");
        sb.append(this.scoreA);
        sb.append('\n');
        sb.append("ScoreB: ");
        sb.append(this.scoreB);
        sb.append('\n');
        sb.append("ScoreC: ");
        sb.append(this.scoreC);
        sb.append('\n');
        sb.append("ScoreD: ");
        sb.append(this.scoreD);
        sb.append('\n');
        sb.append("ScoreE: ");
        sb.append(this.scoreE);
        sb.append("\n\n");
        sb.append("Total = ScoreA + Score B + ScoreC - ScoreD - ScoreE =\n");
        sb.append("Total = ");
        sb.append(this.scoreA + " + "
                + this.scoreB + " + "
                + this.scoreC + " + "
                + this.scoreD + " + "
                + this.scoreE + "\n\n");
        sb.append("Total solution value:\n");
        sb.append(this.totalScore);
        sb.append("\n\n");
        sb.append("Total number of swaps:\n");
        sb.append(this.numberOfSwaps);
        sb.append("\n");

        return sb.toString();
    }



}
