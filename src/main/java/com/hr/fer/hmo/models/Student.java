package com.hr.fer.hmo.models;

import java.util.HashMap;

public class Student {

    private int studentId;
    private HashMap<Integer, Activity> activitiesById;

    public Student(int studentId){
        this.studentId  = studentId;
        activitiesById = new HashMap<Integer, Activity>();
    }

    public Student(Student student) {
         this.studentId = student.getStudentId();
         activitiesById = new HashMap<Integer, Activity>();
         for (Activity activity : student.getActivities().values()) {
             activitiesById.put(activity.getActivityId(), new Activity(studentId, activity.getActivityId(), activity.getOldGroupId(), activity.getSwapWeight()));
             for (Integer request : activity.getRequestedGroupIds()) {
                 activitiesById.get(activity.getActivityId()).addToRequestedGroupIds(request);
             }
         }
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", activities=" + activitiesById.toString() +
                '}';
    }

    public int getStudentId() {
        return studentId;
    }

    public HashMap<Integer, Activity> getActivities() {
        return activitiesById;
    }

    public void addToActivities(Activity activity) {
        activitiesById.put(activity.getActivityId(), activity);
    }

}
