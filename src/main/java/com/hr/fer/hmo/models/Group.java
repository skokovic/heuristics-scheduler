package com.hr.fer.hmo.models;

import java.util.ArrayList;

public class Group {
    private int groupId;
    private int studentsCount;
    private int minStudents;
    private int minPreferredStudents;
    private int maxStudents;
    private int maxPreferredStudents;

    private ArrayList<Integer> overlaps;

    public Group(int groupId, int studentsCount, int minStudents, int minPreferredStudents, int maxStudents, int maxPreferredStudents){
        this.groupId                = groupId;
        this.studentsCount          = studentsCount;
        this.minStudents            = minStudents;
        this.minPreferredStudents   = minPreferredStudents;
        this.maxStudents            = maxStudents;
        this.maxPreferredStudents   = maxPreferredStudents;

        overlaps = new ArrayList<Integer>();

    }

    public Group(Group group) {
        this.groupId = group.getGroupId();
        this.studentsCount = group.getStudentsCount();
        this.minStudents = group.getMinStudents();
        this.minPreferredStudents = group.getMinPreferredStudents();
        this.maxStudents = group.getMaxStudents();
        this.maxPreferredStudents = group.getMaxPreferredStudents();
        this.overlaps = group.getOverlaps();
    }

    public int getGroupId() {
        return groupId;
    }

    public ArrayList<Integer> getOverlaps() {
        return overlaps;
    }

    public void addToOverlaps(int overlapGroupId) {
        this.overlaps.add(overlapGroupId);
    }

    public void reduceStudentsCount() {
        studentsCount--;
    }

    public void increaseStudentsCount() {
        studentsCount++;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupId=" + groupId +
                ", studentsCount=" + studentsCount +
                ", minStudents=" + minStudents +
                ", minPreferredStudents=" + minPreferredStudents +
                ", maxStudents=" + maxStudents +
                ", maxPreferredStudents=" + maxPreferredStudents +
                ", overlaps=" + overlaps +
                '}';
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public int getMinStudents() {
        return minStudents;
    }

    public int getMinPreferredStudents() {
        return minPreferredStudents;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public int getMaxPreferredStudents() {
        return maxPreferredStudents;
    }
}
