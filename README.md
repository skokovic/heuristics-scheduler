# heuristics-scheduler

## Starting the application in a Docker container

>Treating containers like a black box will eventually leave you in the dark. - Kelsey Hightower

_You must install Docker before you continue._

We are using a multi-stage build with maven and openjdk images.
No dependencies on the host are necessary, except Docker.

Position yourself in the root dir of the project.

1.  Build the Docker image

    ```
    docker build -t student-swap .
    ```   

2.  Run the container in detached mode

    ```
    docker run -td -v `pwd`/instances:/data --name scheduler student-swap
    ```
    
    In this case, we're mounting the instances directory in our project dir to /data. Also, we're defining a name for our container for easier access!
    
3.  Execute the program inside the Docker container

    ```
    docker exec -ti -w /data scheduler /bin/sh -c "java -jar /app/scheduler.jar -timeout 600 -students-file instance3/student.csv -requests-file instance3/requests.csv -overlaps-file instance3/overlaps.csv -limits-file instance3/limits.csv"
    ```


## Bash helper scripts

* `change_instance_filenames.sh` -> changes all instance's filenames to be unified
* `run_all_instances.sh` -> runs the application for all instances and all required timeouts
   


